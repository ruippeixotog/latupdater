package com.ecs.android.sample.oauth2;

/**
 * OAuth 2 credentials found in the <a
 * href="https://code.google.com/apis/console">Google apis console</a>.
 */
public final class OAuth2ClientCredentials {

	/** Value of the "Client ID" shown under "Client ID for installed applications". */
	public static final String CLIENT_ID = "893256806802.apps.googleusercontent.com";

	/** Value of the "Client secret" shown under "Client ID for installed applications". */
	public static final String CLIENT_SECRET = "CYFhiIUPDE4NtyYkhknKDrve";

	/** OAuth 2 scope to use */
	public static final String SCOPE = "https://www.googleapis.com/auth/latitude.all.best";

	/** OAuth 2 redirect uri */
	public static final String REDIRECT_URI = "http://localhost";

	/** Latitude API key */
	public static final String API_KEY = "AIzaSyD43p5vw-4nXPv-yDqjvdDlmWtiJacMhTM";
	
	private OAuth2ClientCredentials() {
	}
}
