package net.ruippeixotog.latupdater.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PendingQueueDatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "queue.db";

	public PendingQueueDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE stamp_queue (id INTEGER PRIMARY KEY NOT NULL, "
				+ "timestamp varchar(20), provider varchar(10), latitude decimal,"
				+ " longitude decimal, accuracy decimal, altitude decimal)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
}
