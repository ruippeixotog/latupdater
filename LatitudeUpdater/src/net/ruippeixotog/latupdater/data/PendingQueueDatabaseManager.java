package net.ruippeixotog.latupdater.data;

import java.util.Queue;

import net.ruippeixotog.latupdater.data.entities.LocationStamp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;

public class PendingQueueDatabaseManager {

	private static PendingQueueDatabaseManager instance;

	public static PendingQueueDatabaseManager getInstance(Context context) {
		if (instance == null)
			instance = new PendingQueueDatabaseManager(context);
		return instance;
	}

	private PendingQueueDatabaseHelper dbHelper;

	private PendingQueueDatabaseManager(Context context) {
		dbHelper = new PendingQueueDatabaseHelper(context);
	}

	public void saveQueue(Queue<LocationStamp> queue) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		while (!queue.isEmpty()) {
			LocationStamp stamp = queue.peek();
			ContentValues values = new ContentValues();
			values.put("timestamp", Long.toString(stamp.getTimestamp()));
			values.put("provider", stamp.getLocation().getProvider());
			values.put("latitude", stamp.getLocation().getLatitude());
			values.put("longitude", stamp.getLocation().getLongitude());
			values.put("accuracy", stamp.getLocation().getAccuracy());
			values.put("altitude", stamp.getLocation().getAltitude());

			db.insert("stamp_queue", null, values);
			queue.poll();
		}
		db.close();
	}

	public void loadQueue(Queue<LocationStamp> queue) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		Cursor cur = db.query("stamp_queue", new String[] { "timestamp",
				"provider", "latitude", "longitude", "accuracy", "altitude" },
				null, null, null, null, "timestamp");

		while (cur.moveToNext()) {
			int i = 1;
			Location loc = new Location("db");
			loc.setProvider(cur.getString(i++));
			loc.setLatitude(cur.getDouble(i++));
			loc.setLongitude(cur.getDouble(i++));
			loc.setAccuracy(cur.getFloat(i++));
			loc.setAltitude(cur.getDouble(i++));

			queue.add(new LocationStamp(cur.getLong(0), loc));
		}

		cur.close();

		db.delete("stamp_queue", null, null);
		db.close();
	}
}
