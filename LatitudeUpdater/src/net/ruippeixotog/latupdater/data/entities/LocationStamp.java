package net.ruippeixotog.latupdater.data.entities;

import android.location.Location;

public class LocationStamp {
	private long timestamp;
	private Location location;

	public LocationStamp(long timestamp, Location location) {
		this.timestamp = timestamp;
		this.location = location;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public Location getLocation() {
		return location;
	}
}
