package net.ruippeixotog.latupdater.web;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import net.ruippeixotog.latupdater.data.entities.LocationStamp;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ecs.android.sample.oauth2.OAuth2ClientCredentials;
import com.ecs.android.sample.oauth2.store.CredentialStore;
import com.ecs.android.sample.oauth2.store.SharedPreferencesCredentialStore;
import com.google.api.client.auth.oauth2.draft10.AccessTokenResponse;
import com.google.api.client.googleapis.auth.oauth2.draft10.GoogleAccessProtectedResource;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpRequest;
import com.google.api.client.http.json.JsonHttpRequestInitializer;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.latitude.Latitude;
import com.google.api.services.latitude.LatitudeRequest;
import com.google.api.services.latitude.model.Location;

public class LatitudeApi {
	public static final String TAG = LatitudeApi.class.getSimpleName();

	public static final DateFormat DATE_FORMAT = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

	public static LocationStamp getCurrentLocation(Context context)
			throws IOException {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return getCurrentLocation(new SharedPreferencesCredentialStore(prefs));
	}

	public static LocationStamp getCurrentLocation(
			CredentialStore credentialStore) throws IOException {
		Latitude latitude = getAuthorizedLatitude(credentialStore);
		Location currentLocation = latitude.currentLocation().get().execute();
		return toLocationStamp(currentLocation);
	}

	public static boolean insertLocation(Context context,
			LocationStamp locationStamp) throws IOException {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return insertLocation(new SharedPreferencesCredentialStore(prefs),
				locationStamp);
	}

	public static boolean insertLocation(CredentialStore credentialStore,
			LocationStamp locationStamp) throws IOException {
		Latitude latitude = getAuthorizedLatitude(credentialStore);
		latitude.location().insert(toLatitudeLocation(locationStamp)).execute();
		return true;
	}

	/**
	 * Clears our credentials (token and token secret) from the shared
	 * preferences. We also setup the authorizer (without the token). After
	 * this, no more authorized API calls will be possible.
	 */
	public static void clearCredentials(Context context) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		new SharedPreferencesCredentialStore(prefs).clearCredentials();
	}

	private static Latitude getAuthorizedLatitude(
			CredentialStore credentialStore) {
		JsonFactory jsonFactory = new JacksonFactory();
		HttpTransport transport = new NetHttpTransport();
		HttpRequestInitializer accessProtectedResource = getProtectedResourceInitializer(
				credentialStore, jsonFactory, transport);

		Latitude.Builder builder = Latitude.builder(transport, jsonFactory);
		builder.setHttpRequestInitializer(accessProtectedResource);
		builder.setJsonHttpRequestInitializer(new JsonHttpRequestInitializer() {
			public void initialize(JsonHttpRequest request) {
				LatitudeRequest latRequest = (LatitudeRequest) request;
				latRequest.setKey(OAuth2ClientCredentials.API_KEY);
			}
		});

		return builder.build();
	}

	private static HttpRequestInitializer getProtectedResourceInitializer(
			CredentialStore credentialStore, JsonFactory jsonFactory,
			HttpTransport transport) {
		AccessTokenResponse accessTokenResponse = credentialStore.read();
		return new GoogleAccessProtectedResource(
				accessTokenResponse.accessToken, transport, jsonFactory,
				OAuth2ClientCredentials.CLIENT_ID,
				OAuth2ClientCredentials.CLIENT_SECRET,
				accessTokenResponse.refreshToken);
	}

	private static LocationStamp toLocationStamp(Location currentLocation) {
		long timestamp = Long.valueOf((String) currentLocation
				.get("timestampMs"));

		android.location.Location loc = new android.location.Location(
				(String) null);
		loc.setLatitude(((BigDecimal) currentLocation.get("latitude"))
				.doubleValue());
		loc.setLongitude(((BigDecimal) currentLocation.get("longitude"))
				.doubleValue());
		if (currentLocation.get("accuracy") != null) {
			loc.setAccuracy(((BigDecimal) currentLocation.get("accuracy"))
					.floatValue());
		}
		return new LocationStamp(timestamp, loc);
	}

	private static Location toLatitudeLocation(LocationStamp locationStamp) {
		Location loc = new Location();
		loc.setKind("latitude#location");
		loc.setTimestampMs(locationStamp.getTimestamp());

		android.location.Location androidLoc = locationStamp.getLocation();
		loc.setLatitude(androidLoc.getLatitude());
		loc.setLongitude(androidLoc.getLongitude());
		loc.setAccuracy(androidLoc.getAccuracy());
		loc.setAltitude(androidLoc.getAltitude());
		return loc;
	}
}
