package net.ruippeixotog.latupdater.app;

import java.util.List;
import java.util.Queue;

import net.ruippeixotog.latupdater.data.entities.LocationStamp;

public interface LatitudeUpdaterInterface {

	long getLastUpdateTime();

	List<LocationStamp> getHistory();

	void clearHistory();

	Queue<LocationStamp> getPendingStamps();
	
	void stop();
}
