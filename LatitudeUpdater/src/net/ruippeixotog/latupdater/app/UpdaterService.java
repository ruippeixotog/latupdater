package net.ruippeixotog.latupdater.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import net.ruippeixotog.latupdater.R;
import net.ruippeixotog.latupdater.app.Keys.IntentKeys;
import net.ruippeixotog.latupdater.app.LocationRetriever.LocationRetrievedListener;
import net.ruippeixotog.latupdater.data.PendingQueueDatabaseManager;
import net.ruippeixotog.latupdater.data.entities.LocationStamp;
import net.ruippeixotog.latupdater.web.LatitudeApi;
import net.ruippeixotog.utils.android.service.NotificationCreator;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class UpdaterService extends Service {
	private static final String TAG = "UpdaterService";

	// constants
	public static final long DEFAULT_UPDATE_INTERVAL = 900000;
	private static final long PER_INSERT_INTERVAL = 5000;

	// utility objects
	private NotificationCreator notifications;
	private AlarmManager alarmMgr;
	private ConnectivityManager connMgr;
	private LocationRetriever locationRetr;

	// control flow related objects
	private Thread workerThread;
	private Intent alarmIntent;
	private PendingIntent pendAlarm;
	private IBinder binder;

	// update parameters
	private long sleepTime = DEFAULT_UPDATE_INTERVAL;
	private long lastUpdateTime = -1;

	// data structures
	private List<LocationStamp> history = new ArrayList<LocationStamp>();
	private Queue<LocationStamp> pendingStamps = new LinkedList<LocationStamp>();

	@Override
	public void onCreate() {
		super.onCreate();
		Log.v(TAG, "onCreate() called.");

		// retrieve system services
		notifications = new NotificationCreator(this,
				UpdaterControlActivity.class, R.string.app_name,
				android.R.drawable.ic_notification_overlay);
		alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);
		connMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

		// load pending queue from internal database
		Log.v(TAG, "Loading queue from database...");
		PendingQueueDatabaseManager.getInstance(this).loadQueue(pendingStamps);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);

		// set parameters received in the intent
		if (intent.getExtras() == null)
			intent.putExtras(new Bundle());
		boolean isAlarm = intent.getBooleanExtra(IntentKeys.IS_ALARM, false);

		if (isAlarm) {
			pendAlarm = null;
			Log.v(TAG, "Alarm activated.");
		}

		sleepTime = intent.getExtras().getLong(IntentKeys.UPDATE_FREQ,
				DEFAULT_UPDATE_INTERVAL);

		if (locationRetr == null) {
			locationRetr = new LocationRetriever(intent.getExtras().getLong(
					IntentKeys.LOC_TIMEOUT, -1));
		}

		if (!isAlarm) {
			Log.v(TAG, "onStartCommand() called, interval = " + sleepTime
					+ ", locationRetr = " + locationRetr.getTimeout());
		}

		// start thread for retrieving and updating location
		if (workerThread != null && workerThread.isAlive())
			workerThread.interrupt();
		workerThread = new ServiceWorker();
		workerThread.start();

		if (!isAlarm)
			notifications.showNotification(R.string.msg_notif_start);

		return START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.v(TAG, "onBind() called.");
		if (binder == null)
			binder = new UpdaterServiceBinder();
		return binder;
	}

	@Override
	public void onDestroy() {
		Log.v(TAG, "onDestroy() called.");

		// interrupt worker thread, if running
		Log.v(TAG, "Interrupting threads...");
		if (workerThread != null) {
			if (workerThread.isAlive())
				workerThread.interrupt();
			workerThread = null;
		}

		notifications.getNotificationManager().cancelAll();

		Log.v(TAG, "Saving queue in database...");
		PendingQueueDatabaseManager.getInstance(this).saveQueue(pendingStamps);

		notifications.showNotification(R.string.msg_notif_stop);
		super.onDestroy();
	}

	private boolean isConnectedToWifi() {
		return connMgr == null ? false : connMgr.getNetworkInfo(
				ConnectivityManager.TYPE_WIFI).isConnected();
	}

	private void updateLatitude() {
		try {
			while (true) {
				synchronized (pendingStamps) {
					if (pendingStamps.isEmpty())
						break;
					LocationStamp stamp = pendingStamps.peek();
					Log.v(TAG,
							"Sending location stamp at " + stamp.getTimestamp()
									+ "...");
					LatitudeApi.insertLocation(this, stamp);
					Log.v(TAG, "Location stamp sent.");

					history.add(stamp);
					pendingStamps.poll();
					Thread.sleep(PER_INSERT_INTERVAL);
				}
			}
		} catch (Exception e) {
			Log.v(TAG, "Error sending location stamp.");
		}

	}

	private Intent getAlarmIntent() {
		if (alarmIntent == null) {
			alarmIntent = new Intent(this, UpdaterService.class);
			alarmIntent.putExtra(IntentKeys.IS_ALARM, true);
			alarmIntent.putExtra(IntentKeys.UPDATE_FREQ, sleepTime);
			alarmIntent.putExtra(IntentKeys.LOC_TIMEOUT,
					locationRetr.getTimeout());
		}
		return alarmIntent;
	}

	private class ServiceWorker extends Thread implements
			LocationRetrievedListener {
		private static final String TAG = "ServiceWorker";

		@Override
		public void run() {
			try {
				// launch location retrieval process, with this object as
				// location retrieved listener
				locationRetr.getLocation(UpdaterService.this, this);

				// wait for a notify() from location retrieval
				synchronized (this) {
					wait();
				}

			} catch (InterruptedException e) {
				// if thread was interrupted, finish without renewing the alarm
				Log.v(TAG, "Worker thread interrupted before location"
						+ " could be retrieved. Alarm not renewed.");
				return;
			}

			// renew the alarm for service wakeup
			setAlarm();
		}

		@Override
		public void locationRetrieved(Location location, boolean isCurrent) {
			// if the thread is no longer alive, do nothing
			if (!isAlive())
				return;

			// set last update time
			lastUpdateTime = System.currentTimeMillis();

			if (!isCurrent || location == null) {
				// show notification if no location could be retrieved by any
				// method
				notifications.showNotification("Could not retrieve current"
						+ " location.");
			} else {
				// add found location to the pending queue
				Log.v(TAG,
						"Got location! provider=" + location.getProvider()
								+ " lat=" + location.getLatitude() + " lon="
								+ location.getLongitude() + " prec="
								+ location.getAccuracy());
				synchronized (pendingStamps) {
					pendingStamps.add(new LocationStamp(lastUpdateTime,
							location));
				}

				// if wifi connection is available, process queue
				if (isConnectedToWifi())
					updateLatitude();
				else
					Log.v(TAG, "Device not connected to the network, stamp"
							+ " added to pending queue.");
			}

			// notify main thread that location retrieval is complete
			synchronized (this) {
				notify();
			}
		}

		private void setAlarm() {
			pendAlarm = PendingIntent.getService(UpdaterService.this, 0,
					getAlarmIntent(), PendingIntent.FLAG_UPDATE_CURRENT);
			alarmMgr.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
					+ sleepTime, pendAlarm);
			Log.v(TAG, "Wakeup alarm set to " + sleepTime + "ms from now.");
		}
	}

	private class UpdaterServiceBinder extends Binder implements
			LatitudeUpdaterInterface {

		@Override
		public long getLastUpdateTime() {
			return lastUpdateTime;
		}

		@Override
		public List<LocationStamp> getHistory() {
			return Collections.unmodifiableList(history);
		}

		@Override
		public void clearHistory() {
			history.clear();
		}

		@Override
		public Queue<LocationStamp> getPendingStamps() {
			return pendingStamps;
		}

		@Override
		public void stop() {
			Log.v(TAG, "Cancelling alarms...");
			alarmMgr.cancel(pendAlarm);
			stopSelf();
		}
	}
}