package net.ruippeixotog.latupdater.app;

import net.ruippeixotog.latupdater.R;
import android.os.Bundle;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;

public class ChooseLocationActivity extends MapActivity {
	private static final String TAG = ChooseLocationActivity.class
			.getSimpleName();

	private MapView mapView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		drawActivity();
	}

	private void drawActivity() {
		setContentView(R.layout.choose_location);
		mapView = (MapView) findViewById(R.id.mapview);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}
