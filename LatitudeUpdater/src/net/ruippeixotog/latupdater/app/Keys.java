package net.ruippeixotog.latupdater.app;

public final class Keys {

	public static final class PrefKeys {

		public static final String UPDATE_FREQ = "updateFreq";
		public static final String LOC_TIMEOUT = "locTimeout";

		private PrefKeys() {
		}
	}

	public static final class IntentKeys {

		public static final String UPDATE_FREQ = "updateFreq";
		public static final String LOC_TIMEOUT = "locTimeout";
		public static final String IS_ALARM = "isAlarm";

		private IntentKeys() {
		}
	}

	private Keys() {
	}
}
