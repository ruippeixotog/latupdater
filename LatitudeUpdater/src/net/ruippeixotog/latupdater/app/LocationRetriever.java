package net.ruippeixotog.latupdater.app;

import java.util.Timer;
import java.util.TimerTask;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

public class LocationRetriever {
	private static final String TAG = "LocationRetriever";

	public static final long DEFAULT_TIMEOUT = 20000;

	private Timer timer;
	private long timeout = DEFAULT_TIMEOUT;

	private LocationManager locMgr;
	private LocationRetrievedListener locResult;

	private boolean isGpsEnabled = false;
	private boolean isNetworkEnabled = false;
	private Location foundNetworkLoc = null;

	public LocationRetriever() {
	}

	public LocationRetriever(long timeout) {
		if (timeout > 0)
			this.timeout = timeout;
	}

	public boolean getLocation(Context context, LocationRetrievedListener result) {
		Log.v(TAG, "Retrieving location...");

		// I use LocationResult callback class to pass location value from
		// MyLocation to user code.
		locResult = result;
		if (locMgr == null)
			locMgr = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);

		// exceptions will be thrown if provider is not permitted.
		try {
			isGpsEnabled = locMgr
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
			isGpsEnabled = false;
		}
		try {
			isNetworkEnabled = locMgr
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
			isNetworkEnabled = false;
		}

		// don't start listeners if no provider is enabled
		if (!isGpsEnabled && !isNetworkEnabled) {
			Log.v(TAG, "No location providers activated.");
			return false;
		}

		foundNetworkLoc = null;
		if (isGpsEnabled)
			locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
					locationListenerGps, Looper.getMainLooper());
		if (isNetworkEnabled)
			locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
					0, locationListenerNetwork, Looper.getMainLooper());
		timer = new Timer();
		timer.schedule(new GetLastLocation(), timeout);
		return true;
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	private LocationListener locationListenerGps = new LocationListener() {
		public void onLocationChanged(Location location) {
			timer.cancel();
			Log.v(TAG, "GPS location found.");
			locResult.locationRetrieved(location, true);
			locMgr.removeUpdates(this);
			locMgr.removeUpdates(locationListenerNetwork);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	private LocationListener locationListenerNetwork = new LocationListener() {
		public void onLocationChanged(Location location) {
			Log.v(TAG, "Network location found.");
			foundNetworkLoc = location;
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	private class GetLastLocation extends TimerTask {
		@Override
		public void run() {
			locMgr.removeUpdates(locationListenerGps);
			locMgr.removeUpdates(locationListenerNetwork);
			if (foundNetworkLoc != null) {
				locResult.locationRetrieved(foundNetworkLoc, true);
				return;
			}

			Log.v(TAG, "Timeout. Trying to retrieve last location...");
			Location netLoc = null, gpsLoc = null;
			if (isGpsEnabled)
				gpsLoc = locMgr
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if (isNetworkEnabled)
				netLoc = locMgr
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

			// if there are both values use the latest one
			if (gpsLoc != null) {
				if (netLoc != null)
					locResult.locationRetrieved(gpsLoc.getTime() > netLoc
							.getTime() ? gpsLoc : netLoc, false);
				else
					locResult.locationRetrieved(gpsLoc, false);

			} else if (netLoc != null) {
				locResult.locationRetrieved(netLoc, false);
			} else
				locResult.locationRetrieved(null, false);
		}
	}

	public static interface LocationRetrievedListener {
		void locationRetrieved(Location location, boolean isCurrent);
	}
}