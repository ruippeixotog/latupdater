package net.ruippeixotog.latupdater.app;

import net.ruippeixotog.latupdater.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class LatitudeUpdaterMenuActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_menu);
	}

	public void showServiceControl(View v) {
		Intent intent = new Intent(this, UpdaterControlActivity.class);
		startActivity(intent);
	}

	public void addPastLocation(View v) {
		Intent intent = new Intent(this, ChooseLocationActivity.class);
		startActivity(intent);
	}
}
