package net.ruippeixotog.latupdater.app;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.ruippeixotog.latupdater.R;
import net.ruippeixotog.latupdater.app.Keys.IntentKeys;
import net.ruippeixotog.latupdater.app.Keys.PrefKeys;
import net.ruippeixotog.latupdater.data.entities.LocationStamp;
import net.ruippeixotog.latupdater.utils.DelayedActionServiceConnection;
import net.ruippeixotog.latupdater.web.LatitudeApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.ecs.android.sample.oauth2.OAuthAccessTokenActivity;

public class UpdaterControlActivity extends Activity {
	private static final String TAG = UpdaterControlActivity.class
			.getSimpleName();

	public static final DateFormat TIMESTAMP_FORMAT = new SimpleDateFormat(
			"MMM dd, HH:mm");

	private TextView textView;

	private ServiceConnection servConn;
	private LatitudeUpdaterInterface service;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.updater_control);

		// set default values for service parameters
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		EditText et = (EditText) findViewById(R.id.update_freq);
		et.setText(Long.toString(prefs.getLong(PrefKeys.UPDATE_FREQ,
				UpdaterService.DEFAULT_UPDATE_INTERVAL) / 1000));

		et = (EditText) findViewById(R.id.timeout);
		et.setText(Long.toString(prefs.getLong(PrefKeys.LOC_TIMEOUT,
				LocationRetriever.DEFAULT_TIMEOUT) / 1000));

		// Performs an authorized API call for testing
		textView = (TextView) findViewById(R.id.response_code);
		textView.setText("Testing API call...");

		new Thread() {
			@Override
			public void run() {
				String message;
				try {
					message = getStampString(LatitudeApi
							.getCurrentLocation(UpdaterControlActivity.this));
				} catch (IOException e) {
					message = "Error occured : " + e.getMessage();
				}

				final String tvText = message;
				textView.post(new Runnable() {
					public void run() {
						textView.setText(tvText);
					}
				});
			}
		}.start();
	}

	/*
	 * Launch the OAuth flow to get an access token required to do authorized
	 * API calls. When the OAuth flow finishes, we redirect to this Activity to
	 * perform the API call.
	 */
	public void oAuthAuthenticate(View v) {
		startActivity(new Intent(this, OAuthAccessTokenActivity.class));
	}

	/*
	 * Clears the credentials and performing an API call to see the unauthorized
	 * message.
	 */
	public void clearCredentials(View v) {
		LatitudeApi.clearCredentials(UpdaterControlActivity.this);
		try {
			textView.setText(getStampString(LatitudeApi
					.getCurrentLocation(UpdaterControlActivity.this)));
		} catch (IOException e) {
			textView.setText("Error occured : " + e.getMessage());
		}
	}

	public void startService(View v) {
		if (isServiceRunning(UpdaterService.class)) {
			Log.v(TAG, "Service already running.");
			Toast.makeText(this, "Service already running.", Toast.LENGTH_SHORT)
					.show();
			return;
		}

		Intent intent = new Intent(getApplicationContext(),
				UpdaterService.class);

		EditText et = (EditText) findViewById(R.id.update_freq);
		try {
			long updateInterval = Long.parseLong(et.getText().toString()) * 1000;
			intent.putExtra(IntentKeys.UPDATE_FREQ, updateInterval);
		} catch (NumberFormatException e) {
		}

		et = (EditText) findViewById(R.id.timeout);
		try {
			long locTimeout = Long.parseLong(et.getText().toString()) * 1000;
			intent.putExtra(IntentKeys.LOC_TIMEOUT, locTimeout);
		} catch (NumberFormatException e) {
		}

		Log.v(TAG, "Starting service...");
		String message = startService(intent) != null ? "Service started sucessfully."
				: "Error starting service.";
		Log.v(TAG, message);
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	public void stopService(View v) {
		if (!isServiceRunning(UpdaterService.class)) {
			Log.v(TAG, "Service not running.");
			Toast.makeText(this, "Service not running.", Toast.LENGTH_SHORT)
					.show();
			return;
		}

		Log.v(TAG, "Stopping service...");
		doServiceAction(new Runnable() {
			@Override
			public void run() {
				service.stop();

				Log.v(TAG, "Unbinding service interface...");
				if (service != null) {
					unbindService(servConn);
					service = null;
				}

				Log.v(TAG, "Service stopped sucessfully.");
				Toast.makeText(UpdaterControlActivity.this,
						"Service stopped sucessfully.", Toast.LENGTH_SHORT)
						.show();
			}
		});

		/*
		 * Log.v(TAG, "Stopping service..."); String message = stopService(new
		 * Intent(getApplicationContext(), UpdaterService.class)) ?
		 * "Service stopped sucessfully." : "Error stopping service.";
		 */
	}

	public void getRecentActivity(View v) {
		doServiceAction(new Runnable() {
			@Override
			public void run() {
				TableLayout tl = (TableLayout) findViewById(R.id.table_activity);
				tl.removeViews(1, tl.getChildCount() - 1);

				for (LocationStamp stamp : service.getHistory())
					tl.addView(inflateActivityRow(stamp,
							R.layout.table_row_history));

				for (LocationStamp stamp : service.getPendingStamps())
					tl.addView(inflateActivityRow(stamp,
							R.layout.table_row_pending));
			}
		});
	}

	@Override
	public void onDestroy() {

		// if bound, unbind service
		if (service != null) {
			unbindService(servConn);
			service = null;
		}

		// save values for service parameters
		SharedPreferences.Editor prefs = PreferenceManager
				.getDefaultSharedPreferences(this).edit();

		EditText et = (EditText) findViewById(R.id.update_freq);
		try {
			prefs.putLong(PrefKeys.UPDATE_FREQ,
					Long.parseLong(et.getText().toString()) * 1000);
		} catch (NumberFormatException e) {
		}

		et = (EditText) findViewById(R.id.timeout);
		try {
			prefs.putLong(PrefKeys.LOC_TIMEOUT,
					Long.parseLong(et.getText().toString()) * 1000);
		} catch (NumberFormatException e) {
		}
		prefs.commit();

		super.onDestroy();
	}

	private boolean isServiceRunning(Class<? extends Service> serviceClass) {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE))
			if (serviceClass.getName().equals(service.service.getClassName()))
				return true;
		return false;
	}

	private void doServiceAction(Runnable action) {
		if (service == null)
			bindUpdater(action);
		else
			action.run();
	}

	private void bindUpdater(Runnable action) {
		if (!isServiceRunning(UpdaterService.class)) {
			Toast.makeText(this, "Service not running.", Toast.LENGTH_SHORT)
					.show();
			return;
		}

		servConn = new DelayedActionServiceConnection(action) {
			@Override
			public void onServiceConnected(ComponentName className,
					IBinder service) {
				UpdaterControlActivity.this.service = (LatitudeUpdaterInterface) service;
				super.onServiceConnected(className, service);
			}

			@Override
			public void onServiceDisconnected(ComponentName name) {
				service = null;
			}
		};

		Log.v(TAG, "Binding service interface...");
		bindService(new Intent(getApplicationContext(), UpdaterService.class),
				servConn, Context.BIND_AUTO_CREATE);
	}

	private String getStampString(LocationStamp stamp) {
		return String.format("Current location: (%.2f, %.2f) on %s", stamp
				.getLocation().getLatitude(), stamp.getLocation()
				.getLongitude(), TIMESTAMP_FORMAT.format(new Date(stamp
				.getTimestamp())));
	}

	private TableRow inflateActivityRow(LocationStamp stamp, int rowResId) {
		TableRow tr = (TableRow) View.inflate(this, rowResId, null);

		TextView tv = (TextView) tr.findViewById(R.id.timestamp);
		tv.setText(TIMESTAMP_FORMAT.format(new Date(stamp.getTimestamp())));

		tv = (TextView) tr.findViewById(R.id.provider);
		tv.setText(stamp.getLocation().getProvider());

		tv = (TextView) tr.findViewById(R.id.latitude);
		tv.setText(String.format("%.2f", stamp.getLocation().getLatitude()));

		tv = (TextView) tr.findViewById(R.id.longitude);
		tv.setText(String.format("%.2f", stamp.getLocation().getLongitude()));

		return tr;
	}
}