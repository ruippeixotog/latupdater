package net.ruippeixotog.latupdater.utils;

import java.util.Arrays;
import java.util.List;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

public class DelayedActionServiceConnection implements ServiceConnection {
	private List<Runnable> connectedActions;
	
	public DelayedActionServiceConnection() {
	}
	
	public DelayedActionServiceConnection(Runnable... connectedActions) {
		this.connectedActions = Arrays.asList(connectedActions);
	}
	
	public void addConnectedAction(Runnable action) {
		connectedActions.add(action);
	}

	@Override
	public void onServiceConnected(ComponentName className, IBinder service) {
		for(Runnable action : connectedActions)
			action.run();
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
	}
}
