package net.ruippeixotog.utils.android.service;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class NotificationCreator {

	private Context context;
	private NotificationManager notificationMgr;
	private Class<? extends Activity> activityClass;
	private String notificationTitle;
	private int iconResId;

	private int notificationFlags = Notification.FLAG_ONLY_ALERT_ONCE;

	public NotificationCreator(Context context) {
		this.context = context;
	}

	public NotificationCreator(Context context,
			Class<? extends Activity> activityClass,
			int notificationTitleResId, int iconResId) {
		this(context, activityClass, context.getString(notificationTitleResId),
				iconResId);
	}

	public NotificationCreator(Context context,
			Class<? extends Activity> activityClass, String notificationTitle,
			int iconResId) {
		this.context = context;
		this.activityClass = activityClass;
		this.notificationTitle = notificationTitle;
		this.iconResId = iconResId;
	}
	
	public void showNotification(String message) {
		showNotification(notificationTitle, message);
	}

	public void showNotification(int msgResId) {
		showNotification(notificationTitle, context.getString(msgResId));
	}

	public void showNotification(String title, int msgResId) {
		showNotification(title, context.getString(msgResId));
	}

	public void showNotification(int titleResId, String message) {
		showNotification(context.getString(titleResId), message);
	}

	public void showNotification(int titleResId, int msgResId) {
		showNotification(context.getString(titleResId),
				context.getString(msgResId));
	}

	public void showNotification(String title, String message) {
		Notification notification = new Notification(iconResId, message,
				System.currentTimeMillis());
		notification.flags = notificationFlags;

		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				new Intent(context, activityClass), 0);
		notification.setLatestEventInfo(context, notificationTitle, message,
				contentIntent);
		getNotificationManager().notify(0, notification);
	}

	public Class<? extends Activity> getActivityClass() {
		return activityClass;
	}

	public void setActivityClass(Class<? extends Activity> activityClass) {
		this.activityClass = activityClass;
	}

	public String getNotificationTitle() {
		return notificationTitle;
	}

	public void setNotificationTitle(String notificationTitle) {
		this.notificationTitle = notificationTitle;
	}

	public int getIcon() {
		return iconResId;
	}

	public void setIcon(int iconResId) {
		this.iconResId = iconResId;
	}

	public int getNotificationFlags() {
		return notificationFlags;
	}

	public void setNotificationFlags(int notificationFlags) {
		this.notificationFlags = notificationFlags;
	}

	public NotificationManager getNotificationManager() {
		if (notificationMgr == null)
			notificationMgr = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
		return notificationMgr;
	}
}
